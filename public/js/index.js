var nekopara_count;
var nikaitou_count;
var koromo_count;
var yui_count;
var str_mid_username;
var nickname;

function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function() {
                firebase.auth().signOut()
                    .then(function() {
                        alert('Sign Out!')
                    })
                    .catch(function(error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    nekopara = document.getElementById('photo1');
    nekopara.addEventListener('click', function() {
        nekopara_count = 1;
        nikaitou_count = koromo_count = yui_count = 0;
    });
    nikaitou = document.getElementById('photo2');
    nikaitou.addEventListener('click', function() {
        nikaitou_count = 1;
        nekopara_count = koromo_count = yui_count = 0;
    });
    koromo = document.getElementById('photo3');
    koromo.addEventListener('click', function() {
        koromo_count = 1;
        nekopara_count = nikaitou_count = yui_count = 0;
    });
    yui = document.getElementById('photo4');
    yui.addEventListener('click', function() {
        yui_count = 1;
        koromo_count = nikaitou_count = nekopara_count = 0;
    });
    if(nekopara_count)
    {
        str_mid_username = "<img src='img/1.png' alt='' class='mr-2 rounded' style='height:64px; width:64px;'>";
        nickname = "(一姬)";
    }
    else if(nikaitou_count)
    {
        str_mid_username = "<img src='img/2.png' alt='' class='mr-2 rounded' style='height:64px; width:64px;'>";
        nickname = "(二階堂美樹)";
    }
    else if(koromo_count)
    {
        str_mid_username = "<img src='img/3.png' alt='' class='mr-2 rounded' style='height:64px; width:64px;'>";
        nickname = "(天江衣)";
    }
    else if(yui_count)
    {
        str_mid_username = "<img src='img/4.png' alt='' class='mr-2 rounded' style='height:64px; width:64px;'>";
        nickname = "(八木唯)";
    }
    else
    {
        str_mid_username = "<img src='img/UUMs.gif' alt='' class='mr-2 rounded' style='height:64px;'>";
        nickname = "(預設)";
    }
    
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    myName = document.getElementById('Myname');
    post_btn.addEventListener('click', function() {
        if (post_txt.value != "" && myName.value != "") {
            var newpostref = firebase.database().ref('com_list').push();
            newpostref.set({
                email: user_email,
                myName: myName.value,
                data: post_txt.value,
                picture: str_mid_username,
                nickname: nickname
            });
            post_txt.value = "";
        }
    });

        var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'>"

        var str_final_username = "<p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            
        var str_after_content = "</p></div></div>\n";


    var postsRef = firebase.database().ref('com_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;

    postsRef.on('child_added', function(data) {
        second_count += 1;
        if (second_count > first_count) {
            var childData = data.val();
            total_post[total_post.length] = str_before_username + childData.picture + str_final_username + childData.myName + childData.nickname + "</strong>" + childData.data + str_after_content;
            document.getElementById('post_list').innerHTML = total_post.join('');
            first_count = total_post.length;
        }
    });
}

window.onload = function() {
    init();
}