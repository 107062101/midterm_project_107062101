# Software Studio 2020 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : 雀魂聊聊
* Key functions (add/delete)
    1. init
    
* Other functions (add/delete)
    沒有，抱歉，因為當初沒有想這麼多，而且我太晚開始寫，所以我把所有的功能全部寫在同一個function了。
	
## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://chatroom-771fb.web.app/

# Components Description : 
1. Membership Mechanism, Firebase Page, Database的功能我都是從Lab6貼過來使用
2. RWD的部分，我大部分在CSS使用vw、vh來偵測畫面大小，同時改變文字與圖片的大小
3. Topic Key Function的部分，我新開另一個網頁，開頭輸入密碼即可使用，只要跟朋友講好密碼是多少就可以和其他使用者有私密的聊天處。
# Other Functions Description : 
1. 在init中，可以利用滑鼠雙擊下方圖片來更換大頭貼
2. 在內容輸入欄的前方可以輸入自己的暱稱，email個資不外漏

## Security Report (Optional)
